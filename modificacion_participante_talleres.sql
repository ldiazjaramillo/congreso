﻿DROP TABLE app_participante_talleres;

CREATE TABLE app_participante_talleres
(
  id serial NOT NULL,
  participante_id integer NOT NULL,
  taller_id integer NOT NULL,
  CONSTRAINT app_participante_talleres_pkey PRIMARY KEY (id ),
  CONSTRAINT app_participante_talleres_taller_id_fkey FOREIGN KEY (taller_id)
      REFERENCES app_taller (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT participante_id_refs_id_227be4c7 FOREIGN KEY (participante_id)
      REFERENCES app_participante (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION DEFERRABLE INITIALLY DEFERRED,
  CONSTRAINT app_participante_talleres_participante_id_talleres_id_key UNIQUE (participante_id , taller_id )
)
WITH (
  OIDS=FALSE
);
ALTER TABLE app_participante_talleres
  OWNER TO congreso;

-- Index: app_participante_talleres_participante_id

-- DROP INDEX app_participante_talleres_participante_id;

CREATE INDEX app_participante_talleres_participante_id
  ON app_participante_talleres
  USING btree
  (participante_id );

-- Index: app_participante_talleres_taller_id

-- DROP INDEX app_participante_talleres_taller_id;

CREATE INDEX app_participante_talleres_taller_id
  ON app_participante_talleres
  USING btree
  (taller_id );
