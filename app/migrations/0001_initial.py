# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Horario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('hora', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Mail',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('subject', models.CharField(max_length=255)),
                ('to', models.CharField(max_length=255)),
                ('from_email', models.EmailField(max_length=75)),
                ('template', models.CharField(max_length=255)),
                ('data', models.TextField()),
                ('sent', models.BooleanField(default=False)),
                ('date', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(max_length=255, blank=True)),
                ('reject_reason', models.CharField(max_length=255, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Pago',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('banco_id', models.IntegerField()),
                ('tipo_operacion', models.IntegerField()),
                ('codigo', models.CharField(max_length=30)),
                ('rif', models.CharField(max_length=30)),
                ('direccion', models.CharField(max_length=200)),
                ('status', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Participante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('email', models.EmailField(unique=True, max_length=75)),
                ('institucion', models.CharField(default=b'', max_length=140, null=True)),
                ('cargo', models.CharField(default=b'', max_length=140, null=True)),
                ('tlf_oficina', models.CharField(default=b'', max_length=20, null=True)),
                ('tlf_movil', models.CharField(default=b'', max_length=14, null=True)),
                ('token', models.CharField(max_length=6)),
                ('status', models.BooleanField(default=False)),
                ('pago', models.ForeignKey(to='app.Pago', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Scenario',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('image', models.ImageField(upload_to=b'scenarios')),
                ('subtitle', models.CharField(max_length=255)),
                ('description', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Speaker',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('image', models.ImageField(upload_to=b'speakers')),
                ('scenario', models.ForeignKey(to='app.Scenario')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Taller',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=255)),
                ('descripcion', models.TextField()),
                ('facilitador', models.CharField(max_length=255)),
                ('cupos', models.IntegerField()),
                ('horario_dia', models.CharField(max_length=255)),
                ('horario_inicial', models.IntegerField()),
                ('horario_final', models.IntegerField()),
                ('status', models.BooleanField(default=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='TipoParticipante',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('status', models.IntegerField(default=1)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Trabajo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('titulo', models.CharField(max_length=255)),
                ('autor', models.CharField(max_length=255)),
                ('autores', models.TextField()),
                ('archivo', models.FileField(upload_to=b'trabajo')),
                ('status', models.IntegerField()),
                ('scenario', models.ForeignKey(to='app.Scenario')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='participante',
            name='talleres',
            field=models.ManyToManyField(to='app.Taller', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='participante',
            name='tipo_participante',
            field=models.ForeignKey(to='app.TipoParticipante'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='participante',
            name='trabajo',
            field=models.ManyToManyField(to='app.Trabajo', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='participante',
            name='usuario',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL, null=True),
            preserve_default=True,
        ),
    ]
