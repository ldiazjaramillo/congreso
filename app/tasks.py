# -*- coding: utf-8 -*-
from __future__ import absolute_import
import json

from celery import task
from .models import Participante, Mail, Pago
from django.core.mail import EmailMessage
from django.contrib.auth.models import User

@task(max_retries=3)
def register_user(name, email):
    #print "estoy en los tasks"
    try:
        user_registered = Participante.objects.create(name=name, email=email)
    except Exception, e:
        register_user.retry(exc=e, countdown=60)

    return user_registered


@task()
def save_welcome_mail(user_id):
    user = Participante.objects.get(pk=user_id)
    Mail.objects.create(
        subject='Bienvenido al Congreso 2015',
        to=user.email,
        from_email='congresojornadas@gmail.com',
        template='welcome.html',
        data=json.dumps(dict(name=user.name, email=user.email, token=user.token))
    )
    msg = EmailMessage(
        subject='Bienvenido al Congreso 2015',
        from_email="congresojornadas@gmail.com",
        to=[user.email]
    )
    msg.template_name = "welcome"
    link = '<a href="'+'http://congresogeomatica.fii.gob.ve/confirmar/?token='+user.token+'" target="_blank">link</a>'
    msg.global_merge_vars = dict(name=user.name, email=user.email, token=user.token, url=link)
    msg.use_template_subject = True
    msg.use_template_from = True
    msg.send()

@task()
def save_envio_password(token_id,usuario_id,clave_usuario):
    participante = Participante.objects.get(pk=token_id)
    usuario = User.objects.get(pk=usuario_id)
    link = '<a href="'+'http://congresogeomatica.fii.gob.ve/panel" target="_blank">link</a>'
    Mail.objects.create(
        subject='Confirmarción de usuario'.decode("utf-8"),
        to=participante.email,
        from_email='congresojornadas@gmail.com',
        template='clave.html',
        data=json.dumps(dict(name=participante.name, email=participante.email, link=link, clave=clave_usuario))
    )
    msg = EmailMessage(
        subject='Confirmarción de usuario'.decode("utf-8"),
        from_email="congresojornadas@gmail.com",
        to=[participante.email]
    )
    msg.template_name = "clave"
    msg.global_merge_vars = dict(name=participante.name, email=participante.email, link=link, clave=clave_usuario)
    msg.use_template_subject = True
    msg.use_template_from = True
    msg.send()

@task()
def mail_pago(usuario_id):
    participante = Participante.objects.get(pk=usuario_id)
    pago = Pago.objects.get(pk=participante.pago_id)
    usuario = User.objects.get(pk=usuario_id)
    link = '<a href="'+'http://congresogeomatica.fii.gob.ve/panel" target="_blank">link</a>'
    msg = EmailMessage(
        subject='Pago Congreso Geomatica'.decode("utf-8"),
        from_email="congresojornadas@gmail.com",
        to=[EMAIL_ADM, ]
    )
    msg.template_name = "pago"
    msg.global_merge_vars = dict(banco=pago.banco, tipo_operacion=pago.tipo_operacion, codigo=pago.codigo, name=participante.name, email=participante.email, rif=pago.rif, direccion=pago.direccion)
    msg.use_template_subject = True
    msg.use_template_from = True
    msg.send()
