from django.contrib import admin

from app.models import *
from celery import chain
from .tasks import save_welcome_mail

pagos = ['Banco del Tesoro', 'Banco de Venezuela',]

status = ['Enviado', 'Rechazado', 'Aceptado con modificaciones', 'Aceptado']


def send_welcome_mail(self, request, queryset):
    for qs in queryset:
        workflow = chain(save_welcome_mail.s(qs.pk))
        workflow.delay()
    self.message_user(request, "Mensaje enviado con exito")
send_welcome_mail.short_description = "Enviar correo de bienvenida"

def marcar_habilitado(modeladmin, request, queryset):
    queryset.update(status=True)
marcar_habilitado.short_description = "Habilitar seleccionados"

def marcar_deshabilitado(modeladmin, request, queryset):
    queryset.update(status=False)
marcar_deshabilitado.short_description = "Deshabilitar seleccionados"

def banco_name(obj):
    if obj.banco_id == 1:
        return "Banco del Tesoro"
    else:
        return "Banco de Venezuela"
banco_name.short_description = 'Banco'

def status_name(obj):
    if obj.status == 1:
        return "Enviado"
    elif obj.status == 2:
        return "Rechazado"
    elif obj.status == 3:
        return "Aceptado con modificaciones"
    elif obj.status == 4:
        return "Aceptado"
status_name.short_description = 'Status'

def operacion_name(obj):
    if obj.tipo_operacion == 1:
        return "Deposito"
    elif obj.tipo_operacion == 2:
        return "Transferencia"
    else:
        return "S/I"
operacion_name.short_description = 'Tipo de Operacion'

class ParticipanteAdmin(admin.ModelAdmin):
    list_display = ('name', 'email', 'token', 'status')
    search_fields = ['name', 'email']
    actions = [send_welcome_mail, marcar_habilitado, marcar_deshabilitado]

class PagoAdmin(admin.ModelAdmin):
    list_display = ('codigo', operacion_name, banco_name, 'status')
    actions = [marcar_habilitado, marcar_deshabilitado]

class TallerAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'facilitador', 'horario_dia', 'cupos', 'status')
    
class TrabajoAdmin(admin.ModelAdmin):
    list_display = ('titulo', 'scenario', 'autor', 'archivo', status_name)

admin.site.register(Participante, ParticipanteAdmin)
admin.site.register(Pago, PagoAdmin)
admin.site.register(Taller, TallerAdmin)
admin.site.register(Trabajo, TrabajoAdmin)
admin.site.register(TipoParticipante)
admin.site.register(Mail)
admin.site.register(Scenario)


