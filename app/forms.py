from django import forms

from .models import Participante
from models import *


class ParticipanteForm(forms.ModelForm):
	tipo_participante = forms.ModelChoiceField(queryset=TipoParticipante.objects.all(), empty_label='Tipo de participacion', )

	class Meta:
	    model = Participante
	    fields = ('name', 'email', 'tipo_participante', 'institucion', 'cargo', 'tlf_movil')
	    

	def clean_email(self):
	    email = self.cleaned_data.get('email')
	    print self
	    if Participante.objects.filter(email=email).exists():
	        raise forms.ValidationError('Ya existe un usuario registrado con este correo.')

	    return email

	def __init__(self, *args, **kwargs):
		super(ParticipanteForm,self).__init__(*args, **kwargs)
		self.fields['name'].widget.attrs.update({'placeholder': 'Nombre y Apellido'})
		self.fields['email'].widget.attrs.update({'placeholder': 'Correo electronico'})
		self.fields['institucion'].widget.attrs.update({'placeholder': 'Institucion'})
		self.fields['cargo'].widget.attrs.update({'placeholder': 'Cargo'})
		self.fields['tlf_movil'].widget.attrs.update({'placeholder': 'Telefono'})

		for field in self.fields:
			self.fields[field].widget.attrs.update({'class': 'form-control'})
			#self.fields[field].error_messages = {'required': 'Es un campo obligatorio'}

		   

class UserRegisteredForm(forms.Form):
	#name = CharField(max_length=255)
	#email = EmailField()
	#token = CharField(max_length=6)

	def __unicode__(self):
		return self.email

	class Meta:
		model = Participante
		fields = ('name', 'email', )

	def save(self, *args, **kwargs):
		#print self.name
		if not self.id:
			self.token = self.get_token()

			while(Participante.objects.filter(token=self.token).exists()):

				self.token = self.get_token()

		super(Participante, self).save(*args, **kwargs)
		return self.name

	def get_token(self):
		chars = string.ascii_letters + string.digits
		token = ''.join(choice(chars) for i in range(6))

		return token