$(function(){

	$('.formulario').on('submit',function(e){
		e.preventDefault();
		var $form = $(this),
			url = $form.attr('action');
			data = $form.serialize();
		var alerta = alertify.alert('Enviando datos al servidor, por favor espere...').set('closable', false).set('basic', true).set('movable', false);
		$('.btn').addClass('disabled');
		$.post(url,data,function(data, textStatus, response){
			
		})
		.fail(function(data, textStatus, response) {
			//console.log(data);
			alerta.close();
			$('.btn').removeClass('disabled');
			var respuesta = $.makeArray( data.responseJSON );
			$.map( data.responseJSON, function( val, key) {
				console.log(key+": "+val.toString());
				var campo = "";
				if(key == "name") campo = "Nombre: ";
				if(key == "tipo_participante") campo = "Tipo de participante: ";
				if(key == "email") campo = "Email: ";
				if(key == "institucion") campo = "Institucion: ";
                if(key == "cargo") campo = "Cargo: ";
                if(key == "tlf_movil") campo = "Teléfono: ";
				alertify.error(campo+val.toString());
			})
			//texto = JSON.stringify(data.responseJSON.email).replace(',', ', ').replace('[', '').replace(']', '');
			
			//alert(data.responseJSON.email);
			
			//console.log(data.responseText);
		})
		.success(function(data, textStatus, response) {
			alerta.close();
			$('#capa_form').text('Gracias por registrarte '+data.name+', recibiras un correo electronico con las instrucciones para finalizar el registro.').addClass('alert alert-success text-center lead');
		});
	});
	$('.vermas').on('click',function(e){
		e.preventDefault();
		var $boton = $(this);
		var $escenario = $boton.parents('.escenario');
		if(!$escenario.hasClass('active')){
			//var url  = $boton.attr('href');
			/*$.get(url,function(data){
				$escenario.addClass('active');
				$escenario.after(data);
			});*/
		}
		
	});


});