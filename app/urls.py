from django.conf.urls import patterns, url

from .views import *


urlpatterns = patterns('',
    url(regex=r'^$', view=InicioCreateView.as_view(), name='inicio'),
    url(regex=r'^registro$', view=CompletarRegistroView.as_view(), name='registro'),
    url(regex=r'^confirmar/$', view=ConfirmarRegistro.as_view(), name='confirmar'),
    url(regex=r'^login/$', view=login.as_view(), name='login'),
    url(regex=r'^logout/$', view=logout_view, name='logout'),
    url(regex=r'^panel/$', view=panel, name='panel'),
    url(regex=r'^panel/modificar/$', view=modificar, name='modificar'),
    url(regex=r'^panel/cancelar/$', view=cancelar, name='cancelar'),
    url(regex=r'^trabajos/$', view=trabajos, name='trabajos'),
    url(regex=r'^trabajos/cancelar/(\d+)$', view=cancelarTrabajo, name='cancelarTrabajo'),
    url(regex=r'^talleres/$', view=talleres, name='talleres'),
    url(regex=r'^talleres/inscribir$', view=inscribirtalleres, name='inscribirtalleres'),
    url(regex=r'^talleres/cancelar/(\d+)$', view=cancelarTaller, name='cancelarTaller'),
    url(regex=r'^organizadores/$', view=organizadores, name='organizadores'),
    url(regex=r'^organizadores/participantes/$', view=organizadores_participantes, name='organizadores_participantes'),
    url(regex=r'^organizadores/trabajos/$', view=organizadores_trabajos, name='organizadores_trabajos'),
    url(regex=r'^organizadores/participantes/modificar/$', view=modificarparticipantes, name='modificarparticipantes'),
    url(regex=r'^organizadores/trabajos/modificar/$', view=modificartrabajos, name='modificartrabajos'),
    
)
