# -*- coding: utf-8 -*-
import json
from django.shortcuts import render_to_response,get_object_or_404, render
from django.template.context import RequestContext
from django.http import HttpResponseRedirect, HttpResponse
from django.views.generic import CreateView, TemplateView

from celery import chain
from django.forms.models import modelformset_factory
from .models import *

from .forms import ParticipanteForm
from .tasks import register_user, save_welcome_mail, save_envio_password
from forms import *
from django.contrib import auth, messages
from django.contrib.auth import *
from django.contrib.auth.models import *
from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage


class AjaxMixin(object):
    def render_to_json_response(self, context, **response_kwargs):
        data = json.dumps(context)
        response_kwargs['content_type'] = 'application/json'
        return HttpResponse(data, **response_kwargs)


class InicioCreateView(AjaxMixin, CreateView):
    model = Participante
    form_class = ParticipanteForm
    template_name = 'inicio.html'

    def get_context_data(self, **kwargs):
        context = super(InicioCreateView, self).get_context_data(**kwargs)
        scenarios = self.get_scenarios()
        context.update(dict(scenarios=scenarios))

        if self.request.GET.get('token'):
            try:
                user_registered = Participante.objects.get(token=self.request.GET.get('token'))
            except Participante.DoesNotExist:
                context.update(dict(error="No existe un usuario registrado con ese token"))
                pass
            else:
                context.update(dict(name=user_registered.name))

        return context

    def get_scenarios(self):
        return Taller.objects.all()


    def form_valid(self, form):
        name = self.request.POST.get('name')
        email = self.request.POST.get('email')
        institucion = self.request.POST.get('institucion')
        cargo = self.request.POST.get('cargo')
        tlf_movil = self.request.POST.get('tlf_movil')
        tipo_participante = self.request.POST.get('tipo_participante')
        tipoParticipante = TipoParticipante.objects.get(pk=tipo_participante)
        try:
            #print "intento guardar"
            user_registered = Participante.objects.create(name=name, email=email, tipo_participante=tipoParticipante, institucion=institucion, cargo=cargo, tlf_movil=tlf_movil)
        except Exception, e:
            #print "Error"
            #print e
            return self.render_to_json_response(form.errors, status=400)

        #print user_registered.id
        workflow = chain(save_welcome_mail.s(user_registered.id))
        workflow.delay()

        data = dict(name=name)
        return self.render_to_json_response(data)

    def form_invalid(self, form):
        #print form.errors
        return self.render_to_json_response(form.errors, status=400)


class ScenarioTemplateView(AjaxMixin, TemplateView):
    template_name = 'scenario.html'

    def get(self, request, *args, **kwargs):
        context = super(ScenarioTemplateView, self).get_context_data(**kwargs)

        try:
            scenario = Scenario.objects.get(name=kwargs['scenario'])
        except Scenario.DoesNotExits:
            data = dict(error='Scenario not found.')
            return self.render_to_json_response(data, status=404)

        speakers = scenario.speaker_set.all()

        if not speakers:
            return self.render_to_json_response({'error': 'Speakers not found.'}, status=400)


        context.update(dict(speakers=speakers))
        return self.render_to_response(context)

class CompletarRegistroView(AjaxMixin, CreateView):
    model = Participante
    form_class = ParticipanteForm
    template_name = 'completar.html'

    def get_context_data(self, **kwargs):
        context = super(CompletarRegistroView, self).get_context_data(**kwargs)

        if self.request.GET.get('token'):
            try:
                user_registered = Participante.objects.get(token=self.request.GET.get('token'))
            except Participante.DoesNotExist:
                context.update(dict(error="No existe un usuario registrado con ese token"))
                pass
            else:
                context.update(dict(name=user_registered.name, email=user_registered.email, tipo_participante=user_registered.tipo_participante_id))

        return context


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        #if form.is_valid():
        email = self.request.POST.get('email')
        institucion = self.request.POST.get('institucion')
        cargo = self.request.POST.get('cargo')
        telefono = self.request.POST.get('tlf_movil')
        participante = Participante.objects.get(email=email)
        #print participante
        participante.institucion = institucion
        participante.cargo = cargo
        participante.tlf_movil = telefono
        participante.save()
        return HttpResponseRedirect('/')

        #print form.errors
        #return render(request, self.template_name, {'form': form})


    def form_invalid(self, form):
        #print form.errors
        return self.render_to_json_response(form.errors, status=400)

class ConfirmarRegistro(AjaxMixin, TemplateView):
    template_name = 'confirmar.html'

    def get_context_data(self, **kwargs):
        context = super(ConfirmarRegistro, self).get_context_data(**kwargs)
      
        if self.request.GET.get('token'):
            try:
                participante = Participante.objects.get(token=self.request.GET.get('token'))
            except Participante.DoesNotExist:
                context.update(dict(error="No existe un usuario registrado con ese token. Por favor, verifique su correo electronico y acceda con el link proporcionado."))
                pass
                return context
            else: 
                validar= User.objects.filter(email=participante.email)
                if validar:
                    context.update(dict(msj="Este usuario ya se encuentra registrado en el sistema.".decode("utf-8"), tipo="warning"))
                    pass
                    return context
                else:
                    context.update(dict(token=participante.token, name=participante.name, email=participante.email, tipo_participante=participante.tipo_participante, institucion=participante.institucion, cargo=participante.cargo, tlf_movil=participante.tlf_movil))
                    return context        
        else:
            context.update(dict(error="No existe un usuario registrado con ese token. Por favor, verifique su correo electronico y acceda con el link proporcionado."))
            pass
            return context

    def post(self, request, *args, **kwargs):  
        participante = Participante.objects.get(token=request.POST['token'])

        password1= request.POST['password1']
        usuario = User.objects.create_user(username=participante.email,email=participante.email, password=password1, first_name=participante.token)
        usuario.is_staff=True
        usuario.save()
        group = Group.objects.get(id=2)
        usuario.groups.add(group)
        participante.usuario = usuario
        participante.save()
        envio_clave = chain(save_envio_password.s(participante.id,usuario.id,password1))
        envio_clave.delay()
        msj="Su registro en el sistema ha sido confirmado con exito! Recibira un correo con la información de acceso al sistema para su respaldo.".decode("utf-8")
        return render(request,"confirmar.html", {'msj':msj, 'tipo':'success'})


@login_required
def panel(request):
    template = 'panel.html'
    user = request.user
    user_groups = user.groups.all()[0].id

    """
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")
    """

    participante = Participante.objects.get(usuario_id=user.id) 
    if request.method == 'POST':
        #paso de datos del pago
        banco_id = request.POST['banco_id']
        tipo_operacion = request.POST['tipo_operacion']
        codigo = request.POST['codigo']
        rif = request.POST['rif']
        direccion = request.POST['direccion']
        pago = Pago.objects.create(banco_id=banco_id,tipo_operacion=tipo_operacion, codigo=codigo, rif=rif, direccion=direccion)
        pago.save()
        participante.pago = pago 
        participante.save()

    if participante.tipo_participante_id == 2 :
        inf_trabajo = True
    else:
        inf_trabajo = False 

    if participante.pago_id:
        pago = Pago.objects.get(id=participante.pago_id)

    if not participante.status and not participante.pago_id:
        msj="Falta el registro de pago"
        modificar= False
    elif ((not participante.status) and (participante.pago_id)):
        msj="Pago por confirmar"
        modificar= True
    else:
        msj="Participante Habilitado"


    return render_to_response(template,context_instance=RequestContext(request,locals()))
    

def logout_view(request):
    auth.logout(request)
    return HttpResponseRedirect("/login")



@login_required
def modificar(request):
    template = 'panel.html'
    user = request.user

    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 

    if request.method == 'POST': 
        banco_id = request.POST['banco_id']
        tipo_operacion = request.POST['tipo_operacion']
        codigo = request.POST['codigo']
        nacionalidad = request.POST['nacionalidad']
        cedula = request.POST['cedula']
        direccion = request.POST['direccion']
        pago = Pago.objects.get(id=participante.pago_id)
        pago.banco_id = banco_id
        pago.tipo_operacion = tipo_operacion
        pago.codigo= codigo
        pago.nacionalidad = nacionalidad
        pago.cedula = cedula
        pago.direccion = direccion
        pago.save()

        if participante.pago_id:
            pago = Pago.objects.get(id=participante.pago_id)

        if participante.tipo_participante_id == 2 :
            inf_trabajo = True
        else:
            inf_trabajo = False 

        if ((not participante.status) and (participante.pago_id)):
            msj="Pago por confirmar"
            modificar= True
            return HttpResponseRedirect("/panel")
        else:
            return render_to_response(template,context_instance=RequestContext(request,locals()))
    else:
        pago = Pago.objects.get(id=participante.pago_id)
        modif = True
        msj="Pago por confirmar"
        return render_to_response(template,context_instance=RequestContext(request,locals()))
 

    
@login_required
def cancelar(request):
    template = 'panel.html'
    user = request.user

    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 
    pagoid = participante.pago_id
    participante.pago_id = ""
    participante.save()
    pago = Pago.objects.get(id=pagoid).delete()

    participante = Participante.objects.get(usuario_id=user.id) 
    if not participante.status and not participante.pago_id:
        return HttpResponseRedirect("/panel")
    else:
        return render_to_response(template,context_instance=RequestContext(request,locals()))



class login(AjaxMixin, TemplateView):
    template_name = 'login.html'

    def post(self, request, *args, **kwargs):  
        username = request.POST['username']
        password = request.POST['password']
        user = auth.authenticate(username=username, password=password)
        if user is not None and user.is_active:
            auth.login(request,user)
            user_groups = user.groups.all()[0].id
            if user_groups == 2:
                return HttpResponseRedirect("/panel")
            elif user_groups == 1:
                return HttpResponseRedirect("/organizadores")
        else:
            error="Correo electronico y/o contraseña incorrectos"
            return render(request,"login.html",{'error':error})


@login_required
def trabajos (request):
    template_name = 'trabajos.html'
    user = request.user

    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 
    scenario = Scenario.objects.all()
    if request.method == 'POST':
        #paso de informacion del trabajo
        titulo = request.POST['titulo']
        scenario = request.POST['scenario']
        autor = request.POST['autor']
        autores = request.POST['autores']
        archivo = request.POST['archivo']
        scenarioid = Scenario.objects.get(name=scenario) 
        trabajo = Trabajo.objects.create(titulo=titulo,scenario_id=scenarioid.id,autor=autor,autores=autores,archivo=archivo,status=1)
        trabajo.save()
        participante.trabajo.add(trabajo)
        messages.success(request, 'El trabajo ha sido guardado de manera exitosa!')
        destinatarios =['freddyf@fii.gob.ve', 'congresogeomatica@gmail.com']
        
        msg = EmailMessage(
            subject='Nuevo Paper Registrado en el Sistema'.decode("utf-8"),
            from_email="congresojornadas@gmail.com",
            to= destinatarios
        )
        msg.template_name = "nuevo-paper"
        msg.global_merge_vars = dict(participante=participante.name, titulo=trabajo.titulo, tema=scenarioid.name, autor=trabajo.autor)
        msg.use_template_subject = True
        msg.use_template_from = True
        msg.send()

    if participante.tipo_participante_id != 2:
        return HttpResponseRedirect("/panel")
    else:
        inf_trabajo = True
        if participante.trabajo.all():
            trabajos = participante.trabajo.all()
            for trabajo in trabajos:
                if trabajo:
                    if trabajo.status == 1:
                        msj_trabajo = "Enviado"
                    elif trabajo.status == 2:
                        msj_trabajo = "Rechazado"
                    elif trabajo.status == 3:
                        msj_trabajo = "Aceptado con modificacion"
                    elif trabajo.status == 4:
                        msj_trabajo = "Aceptado"

    return render_to_response(template_name,context_instance=RequestContext(request,locals()))

@login_required
def cancelarTrabajo(request, trabajo_id):
    template_name = 'trabajo.html'
    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 
    trabajo =  Trabajo.objects.get(pk=trabajo_id)

    try:
        participante.trabajo.remove(trabajo)
    except Exception, e:
        raise e
    else:
        trabajo.status = 5
        trabajo.save()
        messages.success(request, 'El trabajo ha sido cancelado de manera exitosa!')

    participante_trabajos = participante.trabajo.all()

    return HttpResponseRedirect("/trabajos")

@login_required
def talleres (request):
    template_name = 'talleres.html'
    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 
    talleres =  Taller.objects.all()
    participante_talleres = participante.talleres.all()
    horario =  Horario.objects.all() 

    if participante.tipo_participante_id == 2:
        inf_trabajo = True

    return render_to_response(template_name,context_instance=RequestContext(request,locals()))

@login_required
def inscribirtalleres(request):
    template_name = 'talleres.html'
    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 
    talleres =  Taller.objects.all()
    participante_talleres = participante.talleres.all()
    horario =  Horario.objects.all()

    if participante.tipo_participante_id == 2:
        inf_trabajo = True

    if request.method == 'POST':
        id_taller = request.POST['idtalleres']
        taller_seleccionado = Taller.objects.get(id=id_taller) 

        #validacion de horario
        conteo = taller_seleccionado.horario_inicial # inicio de conteo

        for participante_taller in participante_talleres:
            while conteo <= taller_seleccionado.horario_final:
                if ((conteo > participante_taller.horario_inicial)and(conteo < participante_taller.horario_final)):
                    if taller_seleccionado.horario_dia == participante_taller.horario_dia:
                        error="El taller seleccionado no se puede inscribir porque tiene otro talleres ocupado"

                        return render_to_response(template_name,context_instance=RequestContext(request,locals()))
                conteo += 1        

        participante.talleres.add(id_taller)
        taller_seleccionado.cupos = taller_seleccionado.cupos - 1
        taller_seleccionado.save()
        success="Ha sido inscipto en el taller de forma exitosa!"

    participante_talleres = participante.talleres.all()

    return HttpResponseRedirect("/talleres")

@login_required
def cancelarTaller(request, taller_id):
    template_name = 'talleres.html'
    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 1:
        return HttpResponseRedirect("/organizadores")

    participante = Participante.objects.get(usuario_id=user.id) 
    taller =  Taller.objects.get(pk=taller_id)
    participante.talleres.remove(taller)
    taller.cupos = taller.cupos + 1
    taller.save()

    participante_talleres = participante.talleres.all()

    return HttpResponseRedirect("/talleres")

@login_required
def organizadores (request):
    template_name = 'organizadores.html'
    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 2:
        return HttpResponseRedirect("/panel")
    elif user_groups == 1:
        return render_to_response(template_name,context_instance=RequestContext(request,locals()))


@login_required
def organizadores_participantes (request):
    template_name = 'organizadores.html'
    participante = Participante.objects.all()

    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 2:
        return HttpResponseRedirect("/panel")

    organizador_participantes = True
    organizador_trabajos =False

    return render_to_response(template_name,context_instance=RequestContext(request,locals()))


@login_required
def organizadores_trabajos(request):
    template_name = 'organizadores.html'

    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 2:
        return HttpResponseRedirect("/panel")

    trabajo = Trabajo.objects.all()
    scenario = Scenario.objects.all()
    organizador_trabajos = True
    organizador_participantes = False

    return render_to_response(template_name,context_instance=RequestContext(request,locals()))

@login_required
def modificarparticipantes(request):
    template_name = 'organizadores_participantes.html'

    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 2:
        return HttpResponseRedirect("/panel")

    id_participante = request.POST['id']
    participante = Participante.objects.get(id=id_participante)

    if 'guardar' in request.POST:
        status = request.POST['status']
        participante.status = status
        participante.save()

        # enviar correo al participante 
        # destinatarios =['ldiazj@fii.gob.ve']
        
        # msg = EmailMessage(
        #     subject='Nuevo Paper Registrado en el Sistema'.decode("utf-8"),
        #     from_email="congresojornadas@gmail.com",
        #     to= destinatarios
        # )
        # msg.template_name = "nuevo-paper"
        # msg.global_merge_vars = dict(participante=participante.name, status=status)
        # msg.use_template_subject = True
        # msg.use_template_from = True
        # msg.send()
        return HttpResponseRedirect("/organizadores/participantes/")

    if 'enviar' in request.POST:
        return HttpResponseRedirect("/organizadores/participantes/")

    return render_to_response(template_name,context_instance=RequestContext(request,locals()))

@login_required
def modificartrabajos(request):
    template_name = 'organizadores_trabajos.html'

    user = request.user
    user_groups = user.groups.all()[0].id
    if user_groups == 2:
        return HttpResponseRedirect("/panel")

    id_trabajo = request.POST['id']
    trabajo = Trabajo.objects.get(id=id_trabajo)
    scenario = Scenario.objects.all()
    
    if 'guardar' in request.POST:
        status = request.POST['status']
        trabajo.status= status
        trabajo.save()

        # enviar correo al participante 
        # destinatarios =['ldiazj@fii.gob.ve']
        
        # msg = EmailMessage(
        #     subject='Nuevo Paper Registrado en el Sistema'.decode("utf-8"),
        #     from_email="congresojornadas@gmail.com",
        #     to= destinatarios
        # )
        # msg.template_name = "nuevo-paper"
        # msg.global_merge_vars = dict(participante=participante.name, status=status)
        # msg.use_template_subject = True
        # msg.use_template_from = True
        # msg.send()
        return HttpResponseRedirect("/organizadores/trabajos/")


    return render_to_response(template_name,context_instance=RequestContext(request,locals()))