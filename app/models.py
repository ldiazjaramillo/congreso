import string
from random import choice
from django.db import models
from django.contrib.auth.models import User
from django.core.mail import EmailMessage

class TipoParticipante(models.Model):
    name = models.CharField(max_length=255)
    status = models.IntegerField(default=1)

    def __unicode__(self):
        return self.name

class Scenario(models.Model):
    name = models.CharField(max_length=255)
    image = models.ImageField(upload_to='scenarios')
    subtitle = models.CharField(max_length=255)
    description = models.TextField()

    def __unicode__(self):
        return self.name

class Pago(models.Model):
    banco_id = models.IntegerField()# 1 banco del tesoro, 2 banco de venezuela
    tipo_operacion = models.IntegerField()# 1 es deposito, 2 es transferencia
    codigo = models.CharField(max_length=10)
    rif = models.CharField(max_length=30)
    direccion = models.CharField(max_length=200)
    status = models.BooleanField(default=False)

    def __unicode__(self):
        return self.codigo

    def save(self, *args, **kwargs):
        #print "aqui estoy"
        super(Pago, self).save(*args, **kwargs)
        participante = Participante.objects.get(pk=1)
        pago = self
        #usuario = User.objects.get(pk=usuario_id)
        link = '<a href="'+'http://congresogeomatica.fii.gob.ve/panel" target="_blank">link</a>'
        msg = EmailMessage(
            subject='Pago Congreso Geomatica'.decode("utf-8"),
            from_email="congresojornadas@gmail.com",
            to=['maribelv@fii.gob.ve', 'congresogeomatica@gmail.com']
        )
        if pago.banco_id == 1:
            banco = "Banco del Tesoro"
        else:
            banco = "Banco de Venezuela"

        if pago.tipo_operacion == 1:
            tipo_operacion = "Deposito"
        else:
            tipo_operacion = "Transferencia"

        msg.template_name = "pago"
        msg.global_merge_vars = dict(banco=banco, tipo_operacion=tipo_operacion, codigo=pago.codigo, name=participante.name, email=participante.email, rif=pago.rif, direccion=pago.direccion)
        msg.use_template_subject = True
        msg.use_template_from = True
        msg.send()


class Trabajo(models.Model):
    titulo = models.CharField(max_length=255)
    scenario = models.ForeignKey(Scenario)
    autor = models.CharField(max_length=255)
    autores = models.TextField()
    archivo = models.FileField(upload_to='trabajo')
    status = models.IntegerField()# 1 es enviado, 2 es rechazado, 3 es aceptado con modificacion, 4 aceptado, 5 Cancelado por el participante

    def __unicode__(self):
        return self.titulo

class Horario(models.Model):
    hora = models.CharField(max_length=255)

class Taller(models.Model):
    titulo = models.CharField(max_length=255)
    descripcion = models.TextField()
    facilitador = models.CharField(max_length=255)
    cupos = models.IntegerField()
    status = models.IntegerField() #1 es activo, 2 es inactivo
    horario_dia = models.CharField(max_length=255)
    horario_inicial = models.IntegerField()
    horario_final = models.IntegerField()
    status = models.BooleanField(default=True)

class Participante(models.Model):
    name = models.CharField(max_length=255)
    email = models.EmailField( unique = True)
    institucion = models.CharField(max_length=140, default='', null=True)
    cargo = models.CharField(max_length=140, default='', null=True)
    tlf_oficina = models.CharField(max_length=20, default='', null=True)
    tlf_movil = models.CharField(max_length=14, default='', null=True)
    tipo_participante = models.ForeignKey(TipoParticipante)
    token = models.CharField(max_length=6)
    usuario = models.ForeignKey(User, null=True)# id de user
    pago = models.ForeignKey(Pago, null=True)
    status = models.BooleanField(default=False) # False es pago por confirmar, True es si el pago confirmado
    trabajo = models.ManyToManyField(Trabajo, null = True)# id trabajo
    talleres = models.ManyToManyField(Taller, null = True)# id talleres

    def __unicode__(self):
        return self.email

    def save(self, *args, **kwargs):
        #print "aqui estoy"
        if not self.pk:
            self.token = self.get_token()

            while(Participante.objects.filter(token=self.token).exists()):
                self.token = self.get_token()

        super(Participante, self).save(*args, **kwargs)

    def get_token(self):
        chars = string.ascii_letters + string.digits
        token = ''.join(choice(chars) for i in range(6))

        return token




class Mail(models.Model):
    subject = models.CharField(max_length=255)
    to = models.CharField(max_length=255)
    from_email = models.EmailField()
    template = models.CharField(max_length=255)
    data = models.TextField()
    sent = models.BooleanField(default=False, blank=True)
    date = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=255, blank=True)
    reject_reason = models.CharField(max_length=255, blank=True)

    def __unicode__(self):
        return self.to


class Speaker(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    image = models.ImageField(upload_to='speakers')
    scenario = models.ForeignKey(Scenario)

    def __unicode__(self):
        return self.name


